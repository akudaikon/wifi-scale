#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServerSPIFFS.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <EEPROM.h>
#include <FS.h>
#include <HX711.h>
#include <Ticker.h>
#include <ArduinoJson.h>

#define PIN_LED_RED     D2
#define PIN_LED_GREEN   D1
#define PIN_BUTTON      D5  // active high
#define PIN_HX711_CLK   D7
#define PIN_HX711_DAT   D6

#define LED_RED         0x1
#define LED_GREEN       0x2
#define LED_ORANGE      (LED_RED | LED_GREEN)
#define LED_ALL         (LED_RED | LED_GREEN)

#define AVG_ALPHA(x)        ((uint16_t)(x * 65535))
#define WEIGHT_AVG_ALPHA    AVG_ALPHA(0.25)
#define WEIGHT_AVG_SAMPLES  30
#define WEIGHT_BIT_LOSS     7

#define SETTINGS_REV        0xA0
#define MAX_STRING_LENGTH   128

enum EEPROMSettings
{
  SETTING_INITIALIZED,
  SETTING_MQTT_ENABLE,
  // int32 stored below...
  SETTING_ZERO_FACTOR,
  SETTING_CALIBRATION_FACTOR = SETTING_ZERO_FACTOR + 4,
  SETTING_EMPTY_WEIGHT = SETTING_CALIBRATION_FACTOR + 4,
  SETTING_FULL_WEIGHT = SETTING_EMPTY_WEIGHT + 4,
  SETTING_DELIM = SETTING_FULL_WEIGHT + 4,
  // Max 128 characters for EEPROM stored strings below...
  SETTING_MDNS_LENGTH,
  SETTING_MDNS,
  SETTING_MQTT_SERVER_LENGTH = SETTING_MDNS + MAX_STRING_LENGTH,
  SETTING_MQTT_SERVER,
  SETTING_MQTT_USER_LENGTH = SETTING_MQTT_SERVER + MAX_STRING_LENGTH,
  SETTING_MQTT_USER,
  SETTING_MQTT_PASSWORD_LENGTH = SETTING_MQTT_USER + MAX_STRING_LENGTH,
  SETTING_MQTT_PASSWORD,
  NUM_OF_SETTINGS = SETTING_MQTT_PASSWORD + MAX_STRING_LENGTH
};

String deviceID;
String mdnsName;
bool mqttEnable = false;
int32_t zeroFactor = 0;
int32_t calibrationFactor = 0;
int32_t emptyWeight = 0;
int32_t fullWeight = 0;
String mqttServer = "";
String mqttUser = "";
String mqttPassword = "";

uint8_t calibIter = 0;
int32_t calibWeight = 0;
bool doEmptyWeightCalib = false;
bool doFullWeightCalib = false;

int32_t weightAvg = 0;
uint8_t weightPercent = 0;

MDNSResponder mdns;
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

WiFiClient mqttClient;
PubSubClient mqtt(mqttClient);

HX711 scale;

uint8_t blinkLEDs = 0;
Ticker blinkLED_tckr;

uint8_t measIter = 0;
int32_t measWeight = 0;
int32_t oldMeasWeight = 0;

bool forceMQTTUpdate = false;
bool firstWeightMeas = false;
bool firmwareUpdating = false;
uint32_t lastScaleMeasure = 0;
uint32_t lastMQTTUpdate = 0;

void setup()
{
  Serial.begin(115200);

  pinMode(PIN_BUTTON, INPUT);
  pinMode(PIN_LED_RED, OUTPUT);
  pinMode(PIN_LED_GREEN, OUTPUT);
  ledOff(LED_ALL);

  WiFiManager wifiManager;

  // Get MAC address and build AP SSID
  uint8_t macAddr[6];
  WiFi.macAddress(macAddr);
  deviceID = "scale-" + String(macAddr[3], HEX) + String(macAddr[4], HEX) + String(macAddr[5], HEX);
  mdnsName = deviceID;

  // Initialize and read EEPROM settings
  EEPROM.begin(NUM_OF_SETTINGS);
  eepromReadSettings();

  // Initialize SPIFFS
  SPIFFS.begin();

  // If button pressed (active high), factory reset
  if (digitalRead(PIN_BUTTON))
  {
    blinkLEDs = LED_RED;
    blinkLED_tckr.attach_ms(250, ledBlink);
    wifiManager.resetSettings();
    EEPROM.write(SETTING_INITIALIZED, 0);
    EEPROM.commit();
    delay(3000);
    ESP.restart();
  }

  // Blink green LED to show connecting to WiFi
  blinkLEDs = LED_GREEN;
  blinkLED_tckr.attach_ms(500, ledBlink);

  // Connect to WiFi
  wifiManager.setAPCallback(wifiConfigModeCallback);
  if (!wifiManager.autoConnect(deviceID.c_str())) ESP.reset();

  mdns.begin(mdnsName.c_str());
  server.on("/settings", HTTP_GET, [](){ handleFileRead("/config.htm"); });
  server.on("/settings", HTTP_POST, saveSettings);
  server.on("/getSettings", getSettings);
  server.on("/getStatus", getStatus);
  server.on("/calib", doCalib);
  server.on("/reset", [&wifiManager]()
  {
    wifiManager.resetSettings();
    EEPROM.write(SETTING_INITIALIZED, 0);
    EEPROM.commit();
    server.send(200, "text/html", F("Clearing all saved settings (including Wifi) and restarting..."));
    delay(3000);
    ESP.restart();
  });
  server.on("/restart", []()
  {
    server.send(200, "text/html", F("<META http-equiv=\"refresh\" content=\"10;URL='/'\">Restarting..."));
    delay(3000);
    ESP.restart();
  });
  server.onNotFound([]()
  {
    if (!handleFileRead(server.uri())) server.send(404, "text/plain", "Not Found");
  });
  httpUpdater.setup(&server);
  httpUpdater.setStartCallback(firmwareUpdateCallback);
  server.begin();

  // Setup MQTT, if enabled
  if (mqttEnable)
  {
    mqtt.setServer(mqttServer.c_str(), 1883);
    mqtt.setBufferSize(1024);
    //mqtt.setCallback(mqttCallback);
    mqttReconnect();
  }

  scale.begin(PIN_HX711_DAT, PIN_HX711_CLK);
  //scale.set_offset(zeroFactor);
  //scale.set_scale(calibrationFactor);

  // Stop blinking green LED
  blinkLED_tckr.detach();
  ledOff(LED_ALL);
}

void loop()
{
  // Handle HTTP
  server.handleClient();

  // If updating firmware, don't do anything else
  if (firmwareUpdating) return;

  // Calibration
  if (doEmptyWeightCalib)
  {
    calibWeight += scale.read() >> WEIGHT_BIT_LOSS;

    if (++calibIter > WEIGHT_AVG_SAMPLES)
    {
      calibWeight /= WEIGHT_AVG_SAMPLES;
      emptyWeight = calibWeight + 10;
      Serial.print("Empty Weight: ");
      Serial.println(emptyWeight);
      eepromWriteI32(SETTING_EMPTY_WEIGHT, emptyWeight);
      EEPROM.commit();
      doEmptyWeightCalib = false;
    }
  }
  else if (doFullWeightCalib) 
  {
    calibWeight += scale.read() >> WEIGHT_BIT_LOSS;

    if (++calibIter > WEIGHT_AVG_SAMPLES)
    {
      calibWeight /= WEIGHT_AVG_SAMPLES;
      fullWeight = calibWeight - 10;
      Serial.print("Full Weight: ");
      Serial.println(fullWeight);
      eepromWriteI32(SETTING_FULL_WEIGHT, fullWeight);
      EEPROM.commit();
      doFullWeightCalib = false;
    }
  }

  // Measure weight and calculate percent
  else if (emptyWeight > 0 && millis() - lastScaleMeasure > 5000)
  {
    measWeight += scale.read() >> WEIGHT_BIT_LOSS;

    if (++measIter > WEIGHT_AVG_SAMPLES)
    {
      measWeight /= WEIGHT_AVG_SAMPLES;
      if (abs(measWeight - oldMeasWeight) > 10) weightAvg = measWeight;
      else weightAvg = calcAvg(measWeight, weightAvg, WEIGHT_AVG_ALPHA);
      oldMeasWeight = measWeight;

      uint8_t percentage = 0;
      if (weightAvg <= emptyWeight) percentage = 0;
      else if (weightAvg >= fullWeight) percentage = 100;
      else percentage = (((weightAvg - emptyWeight) * 100) / (fullWeight - emptyWeight));

      weightPercent = percentage;

      Serial.print(measWeight);
      Serial.print(", ");
      Serial.print(weightAvg);
      Serial.print(" = ");
      Serial.println(weightPercent);

      measIter = 0;
      measWeight = 0;
      firstWeightMeas = true;
      lastScaleMeasure = millis();
    }
  }

  // Don't do anything else until we have our first weight measurement
  if (!firstWeightMeas) return;

  // Set LED color based on weight percent
  if (weightPercent > 50)
  {
    if (blinkLED_tckr.active()) blinkLED_tckr.detach();
    ledOff(LED_RED);
    ledOn(LED_GREEN);
  }
  else if (weightPercent > 25)
  {
    if (blinkLED_tckr.active()) blinkLED_tckr.detach();
    ledOn(LED_ORANGE);
  }
  else if (weightAvg <= (emptyWeight - 20))
  {
    if (!blinkLED_tckr.active())
    {
      blinkLEDs = LED_RED;
      blinkLED_tckr.attach_ms(500, ledBlink);
    }
  }
  else
  {
    if (blinkLED_tckr.active()) blinkLED_tckr.detach();
    ledOff(LED_GREEN);
    ledOn(LED_RED);
  }

  // Handle MQTT, if enabled
  if (mqttEnable)
  {
    if (!mqtt.connected()) mqttReconnect();

    if (mqtt.connected() && (millis() - lastMQTTUpdate > 300000 || forceMQTTUpdate))
    {
      if (emptyWeight > 0 && weightAvg >= (emptyWeight - 20))
      {
        char valueStr[4];
        String topic = "scale/" + deviceID + "/percent";
        itoa(weightPercent, valueStr, 10);
        mqtt.publish(topic.c_str(), valueStr, false);
        forceMQTTUpdate = false;
      }
      lastMQTTUpdate = millis();
      mqtt.loop();
    }
  }
}

void doCalib()
{
  if (server.hasArg("emptyWeight"))
  {
    calibIter = 0;
    calibWeight = 0;
    emptyWeight = 0;
    doEmptyWeightCalib = true;
  }
  else if (server.hasArg("fullWeight"))
  {
    calibIter = 0;
    calibWeight = 0;
    fullWeight = 0;
    doFullWeightCalib = true;
  }
  else 
  {
    server.send(400);
    return;
  }

  server.send(200);
}

void getSettings()
{
  DynamicJsonDocument root(200);

  root["mqttEnable"] = mqttEnable;
  root["zeroFactor"] = zeroFactor;
  root["calibrationFactor"] = calibrationFactor;
  root["emptyWeight"] = emptyWeight;
  root["fullWeight"] = fullWeight;
  root["mdnsName"] = mdnsName;
  root["mqttServer"] = mqttServer;
  root["mqttUser"] = mqttUser;
  root["mqttPassword"] = mqttPassword;

  String buffer;
  serializeJson(root, buffer);

  server.sendHeader("Cache-Control", "no-cache");
  server.send(200, "text/json", buffer);
}

void saveSettings()
{
  String response;

  if (server.hasArg("mdnsName"))
  {
    mdnsName = server.arg("mdnsName");
    eepromWriteString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  }
  if (server.hasArg("mqttEnable"))
  {
    mqttEnable = (server.arg("mqttEnable") == "on" ? true : false);
    EEPROM.write(SETTING_MQTT_ENABLE, mqttEnable);
  }
  if (server.hasArg("mqttServer"))
  {
    mqttServer = server.arg("mqttServer");
    eepromWriteString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, mqttServer);
  }
  if (server.hasArg("mqttUser"))
  {
    mqttUser = server.arg("mqttUser");
    eepromWriteString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, mqttUser);
  }
  if (server.hasArg("mqttPassword"))
  {
    mqttPassword = server.arg("mqttPassword");
    eepromWriteString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, mqttPassword);
  }
  EEPROM.commit();

  response = F("<META http-equiv=\"refresh\" content=\"10;URL='/'\">Settings saved! Restarting to take effect...<br><br>");
  server.send(200, "text/html", response);
  delay(1000);
  ESP.reset();
}

void getStatus()
{
  DynamicJsonDocument root(200);

  root["weightPercent"] = weightPercent;
  root["ip"] = "";

  String buffer;
  serializeJson(root, buffer);

  server.sendHeader("Cache-Control", "no-cache");
  server.send(200, "text/json", buffer);
}

void ledOn(uint8_t leds)
{
  if (leds & LED_RED) digitalWrite(PIN_LED_RED, 0);
  if (leds & LED_GREEN) digitalWrite(PIN_LED_GREEN, 0);
}

void ledOff(uint8_t leds)
{
  if (leds & LED_RED) digitalWrite(PIN_LED_RED, 1);
  if (leds & LED_GREEN) digitalWrite(PIN_LED_GREEN, 1);
}

void ledBlink()
{
  static bool blinkState = false;

  if (blinkState)
  {
    ledOn(blinkLEDs);
    ledOff(blinkLEDs ^ 0xFF);
  }
  else ledOff(LED_ALL);

  blinkState = !blinkState;
}

bool handleFileRead(String path)
{
  if (path.endsWith("/")) path += "index.htm";

  String dataType = "text/plain";
  if (path.endsWith(".htm")) dataType = "text/html";
  else if (path.endsWith(".css")) dataType = "text/css";

  if (SPIFFS.exists(path))
  {
    File file = SPIFFS.open(path, "r");
    server.streamFile(file, dataType);
    file.close();
    return true;
  }
  return false;
}

void MQTT_haDiscovery()
{
  if (!mqtt.connected()) return;

  String dev;
  String topic;

  dev = "\"dev\": { \"name\": \"" + deviceID + "\", \"ids\": \"" + deviceID + "\", \"mdl\": \"Wifi Scale\", \"mf\": \"Akudaikon\", \"cu\": \"http://" + WiFi.localIP().toString() + "\" }";

  String config;
  String availTopic;

  availTopic = "scale/" + deviceID + "/availability";

  topic = "homeassistant/sensor/" + deviceID + "-water/config";
  config = "{\"name\": \"Weight Percent\", \"obj_id\": \""+ deviceID + "-percent\", \"ic\": \"mdi:percent\", \"stat_t\": \"scale/" + deviceID + "/percent\", \"avty_t\": \"" + availTopic + "\", \"unit_of_meas\": \"%\", \"uniq_id\": \"" + deviceID + "-percent\", " + dev + " }";
  mqtt.publish(topic.c_str(), config.c_str(), true);
}

void mqttReconnect()
{
  static long lastReconnect = 0;

  String availTopic = "scale/" + deviceID + "/availability";

  if (millis() - lastReconnect > 5000)
  {
    if (mqtt.connect(deviceID.c_str(), (mqttUser == "" ? NULL : mqttUser.c_str()), (mqttPassword == "" ? NULL : mqttPassword.c_str()), availTopic.c_str(), 1, true, "offline"))
    {
      mqtt.publish(availTopic.c_str(), "online", true);
      MQTT_haDiscovery();
      forceMQTTUpdate = true;
      lastReconnect = 0;
    }
    else lastReconnect = millis();
  }
}

void eepromInitSettings()
{
  EEPROM.write(SETTING_INITIALIZED,   SETTINGS_REV);  // EEPROM initialized
  EEPROM.write(SETTING_MQTT_ENABLE,              0);  // Enable MQTT
  eepromWriteI32(SETTING_ZERO_FACTOR,            0);  // Load cell zero factor
  eepromWriteI32(SETTING_CALIBRATION_FACTOR,     0);  // Load cell calibration factor
  eepromWriteI32(SETTING_EMPTY_WEIGHT,           0);  // Load cell empty weight
  eepromWriteI32(SETTING_FULL_WEIGHT,            0);  // Load cell full weight
  eepromWriteString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  eepromWriteString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, "");
  eepromWriteString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, "");
  eepromWriteString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, "");
  EEPROM.commit();
}

void eepromReadSettings()
{
  if (EEPROM.read(SETTING_INITIALIZED) != SETTINGS_REV) eepromInitSettings();

  mdnsName = eepromReadString(SETTING_MDNS, SETTING_MDNS_LENGTH);
  mqttEnable = EEPROM.read(SETTING_MQTT_ENABLE);
  zeroFactor = eepromReadI32(SETTING_ZERO_FACTOR);
  calibrationFactor = eepromReadI32(SETTING_CALIBRATION_FACTOR);
  emptyWeight = eepromReadI32(SETTING_EMPTY_WEIGHT);
  fullWeight = eepromReadI32(SETTING_FULL_WEIGHT);
  mqttServer = eepromReadString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH);
  mqttUser = eepromReadString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH);
  mqttPassword = eepromReadString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH);
}

int32_t eepromReadI32(uint16_t startAddress)
{
  uint8_t value[4] = { 0 };
  for (int i = 0; i < 2; i++) value[i] = EEPROM.read(startAddress + i);
  return *((int32_t*)value);
}

void eepromWriteI32(uint16_t startAddress, int32_t value)
{
  for (int i = 0; i < 4; i++) EEPROM.write(startAddress + i, ((uint8_t*)&value)[i]);
}

String eepromReadString(uint16_t startAddress, uint16_t lengthAddress)
{
  String returnString = "";
  uint8_t length = constrain(EEPROM.read(lengthAddress), 0, MAX_STRING_LENGTH);
  for (int i = 0; i < length; i++) returnString += (char)EEPROM.read(startAddress + i);
  return returnString;
}

void eepromWriteString(uint16_t startAddress, uint16_t lengthAddress, String str)
{
  uint8_t length = constrain(str.length(), 0, MAX_STRING_LENGTH);
  EEPROM.write(lengthAddress, length);
  for (int i = 0; i < length; i++) EEPROM.write(startAddress + i, str[i]);
  EEPROM.commit();
}

int32_t calcAvg(int32_t value, int32_t average, uint16_t alpha)
{
  int64_t temp = (int64_t)value * (alpha) + (int64_t)average * (65536 - alpha);
  return (int32_t)((temp + 32768) / 65536);
}

void wifiConfigModeCallback(WiFiManager *myWiFiManager)
{
  blinkLEDs = LED_ORANGE;
  blinkLED_tckr.attach_ms(500, ledBlink);
}

void firmwareUpdateCallback()
{
  blinkLEDs = LED_ORANGE;
  blinkLED_tckr.attach_ms(250, ledBlink);
  firmwareUpdating = true;
}