getStatus = function() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var status = JSON.parse(xhttp.responseText);

        document.getElementById("weightPercentText").innerHTML = status.weightPercent + "%";
        document.getElementById("weightPercentBar").style = "width: " + status.weightPercent + "%";

        document.getElementById("weightPercentBar").className = "progress-bar";
        if (status.weightPercent > 50) document.getElementById("weightPercentBar").classList.add("progress-bar-success");
        else if (status.weightPercent > 25) document.getElementById("weightPercentBar").classList.add("progress-bar-warning");
        else document.getElementById("weightPercentBar").classList.add("progress-bar-danger");

        setTimeout(getStatus, 5000);
      }
    }
  };

  xhttp.open("GET", "getStatus", true);
  xhttp.send();
};

// To get around onLoad not firing on back
setTimeout(getStatus, 100);
