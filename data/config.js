var mdnsName = document.getElementById('mdnsName'),
    mqttEnable = document.getElementById('mqttEnable'),
    mqttServer = document.getElementById('mqttServer'),
    mqttUser = document.getElementById('mqttUser'),
    mqttPassword = document.getElementById('mqttPassword');

getSettings = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var settings = JSON.parse(xhttp.responseText);
        mdnsName.value = settings.mdnsName;
        mqttServer.value = settings.mqttServer;
        mqttUser.value = settings.mqttUser;
        mqttPassword.value = settings.mqttPassword;
        mqttEnable.checked = settings.mqttEnable;
      }
    }
  };
  xhttp.open("GET", "getSettings", true);
  xhttp.send();
}

var iter = 0;

doCalib = function(type) {
  var xhttp = new XMLHttpRequest();
  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-time'></span></h1>";

  if (type == 'emptyWeight')
  {
    if (confirm("Ensure object is empty, then click OK to start calibration."))
    {
      iter = 0;
      messageModalText.innerHTML = "<h4>Doing Empty Weight Calibration...</h4>Please wait";
      messageModalProgress.style.display = "block";
      $('#messageModal').modal('show');

      xhttp.open("GET", "calib?emptyWeight", false);
      xhttp.send();

      (function poll() {
        setTimeout(function() {
          $.ajax({
            url: "getSettings",
            type: "GET",
            success: function(settings) {
              if (settings.emptyWeight != 0)
              {
                getSettings();
                $('#messageModal').modal('hide');
              }
              else
              {
                if (++iter < 10) poll();
                else
                {
                  getSettings();
                  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
                  messageModalText.innerHTML = "<h4>Empty Weight Calibration Failed!</h4><br>";
                  messageModalProgress.style.display = "none";
                  setTimeout(function() { $('#messageModal').modal('hide'); }, 5000);
                }
              }
            },
            error: function(e) {
              getSettings();
              messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
              messageModalText.innerHTML = "<h4>Empty Weight Calibration Failed!</h4>Please try again<br>(Error " + e.responseText + ")<br>";
              messageModalProgress.style.display = "none";
              setTimeout(function() { $('#messageModal').modal('hide'); }, 5000);
            },
            dataType: "json",
            timeout: 2000
          })
        }, 2000);
      })();
    }
  }
  else if (type == 'fullWeight')
  {
    if (confirm("Ensure object is filled fully, then click OK to start calibration."))
    {
      iter = 0;
      messageModalText.innerHTML = "<h4>Doing Full Weight Calibration...</h4>Please wait";
      messageModalProgress.style.display = "block";
      $('#messageModal').modal('show');

      xhttp.open("GET", "calib?fullWeight", false);
      xhttp.send();

      (function poll() {
        setTimeout(function() {
          $.ajax({
            url: "getSettings",
            type: "GET",
            success: function(settings) {
              if (settings.fullWeight != 0)
              {
                getSettings();
                $('#messageModal').modal('hide');
              }
              else
              {
                if (++iter < 10) poll();
                else
                {
                  getSettings();
                  messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
                  messageModalText.innerHTML = "<h4>Full Weight Calibration Failed!</h4><br>";
                  messageModalProgress.style.display = "none";
                  setTimeout(function() { $('#messageModal').modal('hide'); }, 5000);
                }
              }
            },
            error: function(e) {
              getSettings();
              messageModalIcon.innerHTML = "<h1><span class='glyphicon glyphicon-alert' style='color: red'></span></h1>";
              messageModalText.innerHTML = "<h4>Full Weight Calibration Failed!</h4>Please try again<br>(Error " + e.responseText + ")<br>";
              messageModalProgress.style.display = "none";
              setTimeout(function() { $('#messageModal').modal('hide'); }, 5000);
            },
            dataType: "json",
            timeout: 2000
          })
        }, 2000);
      })();
    }
  }
}

// To get around onLoad not firing on back
setTimeout(getSettings, 100);